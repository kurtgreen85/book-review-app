from django.urls import path

from reviews.views import list_review, create_review, review_detail

urlpatterns = [
    path("", list_review, name="reviews_list"),
    path("new/", create_review, name="create_review"),
    path("<int:id>/", review_detail, name="review_detail"),
]
